package com.themukha.taskforcebackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TaskForceBackendApplication

fun main(args: Array<String>) {
	runApplication<TaskForceBackendApplication>(*args)
}
