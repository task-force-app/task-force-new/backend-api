package com.themukha.taskforcebackend.controller

import com.themukha.taskforcebackend.data.request.workspace.WorkspaceCreateRequest
import com.themukha.taskforcebackend.data.request.workspace.WorkspaceUpdateRequest
import com.themukha.taskforcebackend.data.response.workspace.WorkspaceCreateResponse
import com.themukha.taskforcebackend.data.response.workspace.WorkspaceUpdateResponse
import com.themukha.taskforcebackend.dto.WorkspaceDto
import com.themukha.taskforcebackend.service.WorkspaceService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/workspaces")
class WorkspaceController(
    private val workspaceService: WorkspaceService
) {

    @GetMapping
    fun getAll(): List<WorkspaceDto> = workspaceService.getAll()

    @GetMapping("/{id}")
    fun getById(
        @PathVariable id: Int
    ): WorkspaceDto = workspaceService.getById(id)

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(
        @RequestBody workspace: WorkspaceCreateRequest
    ): WorkspaceCreateResponse =
        workspaceService.create(workspace) // Assuming .toCreateResponse() exists

    @PutMapping("/{id}")
    fun update(
        @PathVariable id: Int,
        @RequestBody workspace: WorkspaceUpdateRequest
    ): WorkspaceUpdateResponse =
        workspaceService.update(id, workspace) // Assuming .toUpdateResponse() exists

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteById(
        @PathVariable id: Int
    ) = workspaceService.deleteById(id)
}