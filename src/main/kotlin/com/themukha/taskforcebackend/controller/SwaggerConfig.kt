package com.themukha.taskforcebackend.controller

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.info.License
import org.springframework.context.annotation.Configuration

@Configuration
@OpenAPIDefinition(
    info = Info(
        title = "Task Force Backend APIs", version = "1.0",
        description = "APIs for Task Force Backend - backend of the multiplatform task-tracking application",
        license = License(
            name = "SOFTWARE LICENSE AGREEMENT",
            url = "https://gitlab.com/task-force-app/task-force-new/backend-api/-/blob/main/license.md"
        ),
        contact = Contact(
            name = "George Mukha",
            email = "george@themukha.com",
            url = "https://t.me/themukha/"
        )
    )
)
class SwaggerConfig {

}