package com.themukha.taskforcebackend.controller

import com.themukha.taskforcebackend.data.request.user.UserCreateRequest
import com.themukha.taskforcebackend.data.request.user.UserUpdateRequest
import com.themukha.taskforcebackend.data.response.user.UserCreateResponse
import com.themukha.taskforcebackend.data.response.user.UserUpdateResponse
import com.themukha.taskforcebackend.dto.UserDto
import com.themukha.taskforcebackend.service.UserService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/users")
class UserController(
    private val userService: UserService,
) {

    @GetMapping
    fun getAll(): List<UserDto> = userService.getAll()

    @GetMapping("/{id}")
    fun getById(
        @PathVariable id: Int
    ): UserDto = userService.getById(id)

    @PostMapping
    fun create(
        @RequestBody user: UserCreateRequest
    ): UserCreateResponse = userService.create(user)

    @PutMapping("/{id}")
    fun update(
        @PathVariable id: Int,
        @RequestBody user: UserUpdateRequest
    ): UserUpdateResponse = userService.update(id, user)

    @DeleteMapping("/{id}")
    fun deleteById(
        @PathVariable id: Int
    ) = userService.deleteById(id)

}