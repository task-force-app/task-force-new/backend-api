package com.themukha.taskforcebackend.controller

import com.themukha.taskforcebackend.util.logger
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping
class BaseController {

    @GetMapping
    fun get(): String {
        logger(this).warn("Someone reached GET `/` endpoint")
        return "Welcome to Task-Force, traveler!"
    }

    @PostMapping
    fun post(): String {
        logger(this).warn("Someone reached POST `/` endpoint")
        return "Welcome to Task-Force, traveler!"
    }

    @PutMapping
    fun put(): String {
        logger(this).warn("Someone reached PUT `/` endpoint")
        return "Welcome to Task-Force, traveler!"
    }

    @DeleteMapping
    fun delete(): String {
        logger(this).warn("Someone reached DELETE `/` endpoint")
        return "Welcome to Task-Force, traveler!"
    }
}