package com.themukha.taskforcebackend.controller

import com.themukha.taskforcebackend.data.request.task.TaskCreateRequest
import com.themukha.taskforcebackend.data.request.task.TaskUpdateRequest
import com.themukha.taskforcebackend.data.response.task.TaskCreateResponse
import com.themukha.taskforcebackend.data.response.task.TaskUpdateResponse
import com.themukha.taskforcebackend.dto.TaskDto
import com.themukha.taskforcebackend.service.TaskService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/tasks")
class TaskController(
    private val taskService: TaskService,
) {

    @GetMapping
    fun getAll(): List<TaskDto> = taskService.getAll()

    @GetMapping("/{id}")
    fun getById(
        @PathVariable id: Int
    ): TaskDto = taskService.getById(id)

    @PostMapping
    fun create(
        @RequestBody task: TaskCreateRequest
    ): TaskCreateResponse = taskService.create(task)

    @PutMapping("/{id}")
    fun update(
        @PathVariable id: Int,
        @RequestBody task: TaskUpdateRequest
    ): TaskUpdateResponse = taskService.update(id, task)

    @DeleteMapping("/{id}")
    fun deleteById(
        @PathVariable id: Int
    ) = taskService.deleteById(id)
}