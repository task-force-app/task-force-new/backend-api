package com.themukha.taskforcebackend.repository

import com.themukha.taskforcebackend.data.request.task.TaskCreateRequest
import com.themukha.taskforcebackend.data.request.task.TaskUpdateRequest
import com.themukha.taskforcebackend.dto.TaskDto
import com.themukha.taskforcebackend.exception.TaskCanNotBeCreatedException
import com.themukha.taskforcebackend.exception.TaskNotFoundException
import com.themukha.taskforcebackend.model.Task
import com.themukha.taskforcebackend.util.getStringOrNull
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
class TaskRepositoryImpl(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
) : TaskRepository {

    override fun create(task: TaskCreateRequest): Task {
        val keyHolder = GeneratedKeyHolder()
        jdbcTemplate.update(
            """
                INSERT INTO
                tasks (
                    workspace_id,
                    user_id,
                    title,
                    description,
                    is_active,
                    created_at,
                    updated_at
                )
                VALUES (
                    :workspaceId,
                    :userId,
                    :title,
                    :description,
                    :isActive,
                    :createdAt,
                    :updatedAt
                )
            """.trimIndent(),
            MapSqlParameterSource(
                mapOf(
                    "workspaceId" to task.workspaceId,
                    "userId" to task.userId,
                    "title" to task.title,
                    "description" to task.description,
                    "isActive" to true,
                    "createdAt" to LocalDateTime.now(),
                    "updatedAt" to LocalDateTime.now()
                )
            ),
            keyHolder,
            listOf("id").toTypedArray()
        )
        val id = keyHolder.keys?.getValue("id") as? Int ?: throw TaskCanNotBeCreatedException("The error while processing task ID")

        return findById(id) ?: throw TaskCanNotBeCreatedException("User cannot access to workspace with id = `${task.workspaceId}`")
    }

    override fun findAll(): List<Task> {
        return jdbcTemplate.query(
            """
                SELECT *
                FROM tasks
                WHERE is_active = true
                ORDER BY id
            """.trimIndent(),
            ROW_MAPPER
        )
    }

    override fun findById(id: Int): Task? {
        return jdbcTemplate.query(
            """
                SELECT *
                FROM tasks
                WHERE id = :id AND is_active = true
            """.trimIndent(),
            mapOf("id" to id),
            ROW_MAPPER
        ).firstOrNull()
    }

    override fun update(id: Int, updatedTask: TaskUpdateRequest, existingTask: TaskDto): Task {
        jdbcTemplate.update(
            """
                UPDATE tasks
                SET title = :title,
                    description = :description, 
                    updated_at = :updatedAt
                WHERE id = :id AND is_active = true
            """.trimIndent(),
            mapOf(
                "title" to (updatedTask.title ?: existingTask.title),
                "description" to (updatedTask.description ?: existingTask.description),
                "updatedAt" to LocalDateTime.now(),
                "id" to id
            )
        )

        return findById(id) ?: throw TaskNotFoundException(id)
    }

    override fun deleteById(id: Int) {
        findById(id) ?: throw TaskNotFoundException(id)

        jdbcTemplate.update(
            """
                UPDATE tasks
                SET is_active = :isActive, 
                    updated_at = :updatedAt
                WHERE id = :id
            """.trimIndent(),
            mapOf(
                "isActive" to false,
                "updatedAt" to LocalDateTime.now(),
                "id" to id
            )
        )
    }

    companion object {
        val ROW_MAPPER = RowMapper<Task> { rs, _ ->
            Task(
                id = rs.getInt("id"),
                workspaceId = rs.getInt("workspace_id"),
                userId = rs.getInt("user_id"),
                title = rs.getStringOrNull("title"),
                description = rs.getStringOrNull("description"),
                isActive = rs.getBoolean("is_active"),
                createdAt = rs.getObject("created_at", LocalDateTime::class.java),
                updatedAt = rs.getObject("updated_at", LocalDateTime::class.java),
            )
        }
    }
}