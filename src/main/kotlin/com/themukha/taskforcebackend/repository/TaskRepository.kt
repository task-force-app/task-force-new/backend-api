package com.themukha.taskforcebackend.repository

import com.themukha.taskforcebackend.data.request.task.TaskCreateRequest
import com.themukha.taskforcebackend.data.request.task.TaskUpdateRequest
import com.themukha.taskforcebackend.dto.TaskDto
import com.themukha.taskforcebackend.model.Task

interface TaskRepository {
    fun create(task: TaskCreateRequest): Task
    fun findAll(): List<Task>
    fun findById(id: Int): Task?
    fun update(id: Int, updatedTask: TaskUpdateRequest, existingTask: TaskDto): Task
    fun deleteById(id: Int)
}