package com.themukha.taskforcebackend.repository

import com.themukha.taskforcebackend.data.request.workspace.WorkspaceCreateRequest
import com.themukha.taskforcebackend.data.request.workspace.WorkspaceUpdateRequest
import com.themukha.taskforcebackend.model.Workspace

interface WorkspaceRepository {
    fun getAll(): List<Workspace>
    fun findById(id: Int): Workspace?
    fun create(workspace: WorkspaceCreateRequest): Workspace
    fun update(id: Int, workspace: WorkspaceUpdateRequest): Workspace
    fun deleteById(id: Int)
    fun existsByIdAndIsActive(id: Int): Boolean
}