package com.themukha.taskforcebackend.repository

import com.themukha.taskforcebackend.data.request.user.UserCreateRequest
import com.themukha.taskforcebackend.data.request.user.UserUpdateRequest
import com.themukha.taskforcebackend.exception.*
import com.themukha.taskforcebackend.model.User
import com.themukha.taskforcebackend.util.getStringOrNull
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
class UserRepositoryImpl(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
) : UserRepository {
    override fun getAll(): List<User> {
        return jdbcTemplate.query(
            """
                SELECT *
                FROM USERS
                WHERE is_active = true
                ORDER BY id
            """.trimIndent(),
            ROW_MAPPER
        )
    }

    override fun findById(id: Int): User? {
        return jdbcTemplate.query(
            """
                SELECT *
                FROM USERS
                WHERE id = :id AND is_active = true
            """.trimIndent(),
            mapOf(
                "id" to id,
            ),
            ROW_MAPPER
        ).firstOrNull()
    }

    override fun create(user: UserCreateRequest): User {
        val keyHolder = GeneratedKeyHolder()
        jdbcTemplate.update(
            """
                INSERT INTO
                users (
                    email,
                    username,
                    password,
                    first_name,
                    last_name,
                    is_active,
                    created_at,
                    updated_at
                )
                VALUES (
                    :email,
                    :username,
                    :password,
                    :firstName,
                    :lastName,
                    :isActive,
                    :createdAt,
                    :updatedAt
                )
            """.trimIndent(),
            MapSqlParameterSource(
                mapOf(
                    "email" to user.email,
                    "username" to user.username,
                    "password" to user.password,
                    "firstName" to user.firstName,
                    "lastName" to user.lastName,
                    "isActive" to true,
                    "createdAt" to LocalDateTime.now(),
                    "updatedAt" to LocalDateTime.now(),
                )
            ),
            keyHolder,
            listOf("id").toTypedArray()
        )
        val id = keyHolder.keys?.getValue("id") as? Int ?: throw UserCanNotBeCreatedException(email = user.email, user.username)

        return findById(id) ?: throw UserNotFoundException(id)
    }

    override fun update(id: Int, user: UserUpdateRequest): User {
        val currentUser: User = findById(id) ?: throw UserCanNotBeUpdatedException(id, "ID not found")
        jdbcTemplate.update(
            """
            UPDATE users
            SET email = :email,
                username = :username,
                password = :password,
                first_name = :firstName,
                last_name = :lastName,
                updated_at = :updatedAt
            WHERE id = :id
            AND is_active = true
        """.trimIndent(),
            mapOf(
                "email" to (user.email ?: currentUser.email),
                "username" to (user.username ?: currentUser.username),
                "password" to (user.password ?: currentUser.password),
                "firstName" to (user.firstName ?: currentUser.firstName),
                "lastName" to (user.lastName ?: currentUser.lastName),
                "updatedAt" to LocalDateTime.now(),
                "id" to id
            )
        )
        return findById(id)!!
    }

    override fun deleteById(id: Int) {
        if (!existsByIdAndIsActive(id)) throw UserCanNotBeDeletedException(id, "Workspace doesn't exist")

        jdbcTemplate.update(
            """
                UPDATE users
                SET is_active = :isActive,
                    updated_at = :updatedAt
                WHERE id = :id
            """.trimIndent(),
            mapOf(
                "isActive" to false, // don't delete the user with update method
                "updatedAt" to LocalDateTime.now(),
                "id" to id
            )
        )
    }

    override fun existsByIdAndIsActive(userId: Int): Boolean {
        val count: Int? = jdbcTemplate.queryForObject(
            "SELECT count(*) FROM users WHERE id = :id AND is_active = true",
            mapOf("id" to userId),
            Int::class.java
        )
        return count == 1
    }

    private companion object {
        val ROW_MAPPER = RowMapper<User> { rs, _ ->
            User(
                id = rs.getInt("id"),
                email = rs.getString("email"),
                username = rs.getString("username"),
                password = rs.getString("password"),
                firstName = rs.getStringOrNull("first_name"),
                lastName = rs.getStringOrNull("last_name"),
                isActive = rs.getBoolean("is_active"),
                createdAt = rs.getObject("created_at", LocalDateTime::class.java),
                updatedAt = rs.getObject("updated_at", LocalDateTime::class.java),
            )
        }
    }
}