package com.themukha.taskforcebackend.repository

import com.themukha.taskforcebackend.data.request.user.UserCreateRequest
import com.themukha.taskforcebackend.data.request.user.UserUpdateRequest
import com.themukha.taskforcebackend.model.User

interface UserRepository {
    fun getAll(): List<User>
    fun findById(id: Int): User?
    fun create(user: UserCreateRequest): User
    fun update(id: Int, user: UserUpdateRequest): User
    fun deleteById(id: Int)
    fun existsByIdAndIsActive(userId: Int): Boolean
}