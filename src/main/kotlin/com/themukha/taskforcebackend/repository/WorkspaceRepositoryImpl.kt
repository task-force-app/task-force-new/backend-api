package com.themukha.taskforcebackend.repository

import com.themukha.taskforcebackend.data.request.workspace.WorkspaceCreateRequest
import com.themukha.taskforcebackend.data.request.workspace.WorkspaceUpdateRequest
import com.themukha.taskforcebackend.exception.WorkspaceCanNotBeCreatedException
import com.themukha.taskforcebackend.exception.WorkspaceCanNotBeDeletedException
import com.themukha.taskforcebackend.exception.WorkspaceCanNotBeUpdatedException
import com.themukha.taskforcebackend.exception.WorkspaceNotFoundException
import com.themukha.taskforcebackend.model.Workspace
import com.themukha.taskforcebackend.util.getStringOrNull
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.support.GeneratedKeyHolder
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
class WorkspaceRepositoryImpl(
    private val jdbcTemplate: NamedParameterJdbcTemplate,
) : WorkspaceRepository {
    override fun getAll(): List<Workspace> {
        return jdbcTemplate.query(
            """
                SELECT *
                FROM workspaces
                WHERE is_active = true
                ORDER BY id
            """.trimIndent(),
            ROW_MAPPER
        )
    }

    override fun findById(id: Int): Workspace? {
        return jdbcTemplate.query(
            """
                SELECT *
                FROM workspaces
                WHERE id = :id
                AND is_active = true
            """.trimIndent(),
            mapOf("id" to id),
            ROW_MAPPER
        ).firstOrNull()
    }

    override fun create(workspace: WorkspaceCreateRequest): Workspace {
        val keyHolder = GeneratedKeyHolder()
        jdbcTemplate.update(
            """
                INSERT INTO workspaces (
                    name,
                    description,
                    owner_id, 
                    is_active, 
                    created_at, 
                    updated_at
                ) 
                VALUES (
                    :name,
                    :description,
                    :ownerId,
                    :isActive,
                    :createdAt,
                    :updatedAt
                )
            """.trimIndent(),
            MapSqlParameterSource(
                mapOf(
                    "name" to workspace.name,
                    "description" to workspace.description,
                    "ownerId" to workspace.ownerId,
                    "isActive" to true,
                    "createdAt" to LocalDateTime.now(),
                    "updatedAt" to LocalDateTime.now(),
                )
            ),
            keyHolder,
            listOf("id").toTypedArray()
        )

        val id = keyHolder.keys?.getValue("id") as? Int ?: throw WorkspaceCanNotBeCreatedException("ID not generated")
        return findById(id) ?: throw WorkspaceNotFoundException(id)
    }

    override fun update(id: Int, workspace: WorkspaceUpdateRequest): Workspace {
        val currentWorkspace: Workspace = findById(id) ?: throw WorkspaceCanNotBeUpdatedException(id, "ID not found")
        jdbcTemplate.update(
            """
                UPDATE workspaces
                SET name = :name,
                    description = :description,
                    updated_at = :updatedAt
                WHERE id = :id
                AND is_active = true
            """.trimIndent(),
            mapOf(
                "name" to (workspace.name ?: currentWorkspace.name),
                "description" to (workspace.description ?: currentWorkspace.description),
                "updatedAt" to LocalDateTime.now(),
                "id" to id
            )
        )

        return findById(id)!!
    }

    override fun deleteById(id: Int) {
        if (!existsByIdAndIsActive(id)) throw WorkspaceCanNotBeDeletedException(id, "Workspace doesn't exist")
        jdbcTemplate.update(
            """
                UPDATE workspaces
                SET is_active = :isActive,
                    updated_at = :updatedAt
                WHERE id = :id
                AND is_active = true
            """.trimIndent(),
            mapOf(
                "isActive" to false,
                "updatedAt" to LocalDateTime.now(),
                "id" to id
            )
        )
    }

    override fun existsByIdAndIsActive(id: Int): Boolean {
        val count: Int? = jdbcTemplate.queryForObject(
            "SELECT count(*) FROM workspaces WHERE id = :id AND is_active = true",
            mapOf("id" to id),
            Int::class.java
        )
        return count == 1
    }

    companion object {
        val ROW_MAPPER = RowMapper<Workspace> { rs, _ ->
            Workspace(
                id = rs.getInt("id"),
                name = rs.getString("name"),
                description = rs.getStringOrNull("description"),
                ownerId = rs.getInt("owner_id"),
                isActive = rs.getBoolean("is_active"),
                createdAt = rs.getObject("created_at", LocalDateTime::class.java),
                updatedAt = rs.getObject("updated_at", LocalDateTime::class.java),
            )
        }
    }
}