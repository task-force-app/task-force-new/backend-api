package com.themukha.taskforcebackend.exception

import org.springframework.http.HttpStatus

abstract class BaseException(
    val errorCode: Int,
    override val message: String,
    val status: HttpStatus,
): RuntimeException(message)

class UserNotFoundException(id: Int): BaseException(
    errorCode = -1,
    message = "User with id = '$id' not found",
    status = HttpStatus.NOT_FOUND
)

class UserCanNotBeCreatedException(email: String, username: String): BaseException(
    errorCode = -2,
    message = "User with email = '$email' or username = '$username' already exists.",
    status = HttpStatus.CONFLICT
)

class UserCanNotBeUpdatedException(id: Int, reason: String) : BaseException(
    errorCode = -3,
    message = "User with id = '$id' cannot be updated due to: $reason",
    status = HttpStatus.CONFLICT
)

class UserCanNotBeDeletedException(id: Int, reason: String) : BaseException(
    errorCode = -4,
    message = "User with id = '$id' cannot be deleted due to: $reason",
    status = HttpStatus.CONFLICT
)

class TaskNotFoundException(id: Int) : BaseException(
    errorCode = -10,
    message = "Task with id = '$id' not found",
    status = HttpStatus.NOT_FOUND
)

class TaskCanNotBeCreatedException(reason: String) : BaseException(
    errorCode = -11,
    message = "Task cannot be created due to: $reason",
    status = HttpStatus.CONFLICT
)

class TaskCanNotBeUpdatedException(id: Int, reason: String) : BaseException(
    errorCode = -12,
    message = "Task with id = '$id' cannot be updated due to: $reason",
    status = HttpStatus.CONFLICT
)

class TaskCanNotBeDeletedException(id: Int, reason: String) : BaseException(
    errorCode = -13,
    message = "Task with id = '$id' cannot be deleted due to: $reason",
    status = HttpStatus.CONFLICT
)

class WorkspaceNotFoundException(id: Int) : BaseException(
    errorCode = -20,
    message = "Workspace with id = '$id' not found",
    status = HttpStatus.NOT_FOUND
)

class WorkspaceCanNotBeCreatedException(reason: String) : BaseException(
    errorCode = -21,
    message = "Workspace cannot be created due to: $reason",
    status = HttpStatus.CONFLICT
)

class WorkspaceCanNotBeUpdatedException(id: Int, reason: String) : BaseException(
    errorCode = -22,
    message = "Workspace with id = '$id' cannot be updated due to: $reason",
    status = HttpStatus.CONFLICT
)

class WorkspaceCanNotBeDeletedException(id: Int, reason: String) : BaseException(
    errorCode = -23,
    message = "Workspace with id = '$id' cannot be deleted due to: $reason",
    status = HttpStatus.CONFLICT
)