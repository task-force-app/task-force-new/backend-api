package com.themukha.taskforcebackend.exception

data class ApiError(
    val errorCode: Int,
    val message: String,
)
