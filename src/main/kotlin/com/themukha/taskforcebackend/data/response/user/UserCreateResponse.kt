package com.themukha.taskforcebackend.data.response.user

import com.themukha.taskforcebackend.model.User

data class UserCreateResponse(
    val id: Int,
    val email: String,
    val username: String,
    val firstName: String?,
    val lastName: String?,
)

fun User.toCreateResponse(): UserCreateResponse {
    return UserCreateResponse(
        id = id,
        email = email,
        username = username,
        firstName = firstName,
        lastName = lastName,
    )
}