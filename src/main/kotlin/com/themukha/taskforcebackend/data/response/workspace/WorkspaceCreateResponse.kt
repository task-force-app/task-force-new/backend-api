package com.themukha.taskforcebackend.data.response.workspace

import com.themukha.taskforcebackend.model.Workspace

data class WorkspaceCreateResponse(
    val id: Int,
    val name: String,
    val description: String?,
    val ownerId: Int,
)

fun Workspace.toCreateResponse(): WorkspaceCreateResponse {
    return WorkspaceCreateResponse(
        id = id,
        name = name,
        description = description,
        ownerId = ownerId
    )
}
