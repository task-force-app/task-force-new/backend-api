package com.themukha.taskforcebackend.data.response.task

import com.themukha.taskforcebackend.model.Task
import java.time.LocalDateTime

data class TaskCreateResponse(
    val id: Int,
    val workspaceId: Int,
    val userId: Int,
    val title: String? = "",
    val description: String? = "",
    val createdAt: LocalDateTime
)

fun Task.toCreateResponse(): TaskCreateResponse {
    return TaskCreateResponse(
        id = id,
        workspaceId = workspaceId,
        userId = userId,
        title = title,
        description = description,
        createdAt = createdAt
    )
}