package com.themukha.taskforcebackend.data.response.user

import com.themukha.taskforcebackend.model.User
import java.time.LocalDateTime

data class UserUpdateResponse(
    var email: String? = null,
    var username: String? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var isActive: Boolean? = null,
    var createdAt: LocalDateTime? = null,
    var updatedAt: LocalDateTime? = null
)

fun User.toUpdateResponse(): UserUpdateResponse {
    return UserUpdateResponse(
        email = email,
        username = username,
        firstName = firstName,
        lastName = lastName,
        isActive = isActive,
        createdAt = createdAt,
        updatedAt = updatedAt
    )
}