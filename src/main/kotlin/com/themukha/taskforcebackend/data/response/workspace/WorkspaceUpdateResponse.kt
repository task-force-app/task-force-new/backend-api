package com.themukha.taskforcebackend.data.response.workspace

import com.themukha.taskforcebackend.model.Workspace
import java.time.LocalDateTime

data class WorkspaceUpdateResponse(
    val name: String,
    val description: String?,
    val updatedAt: LocalDateTime
)

fun Workspace.toUpdateResponse(): WorkspaceUpdateResponse {
    return WorkspaceUpdateResponse(
        name = name,
        description = description,
        updatedAt = updatedAt
    )
}