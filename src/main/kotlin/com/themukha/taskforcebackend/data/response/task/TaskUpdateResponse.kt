package com.themukha.taskforcebackend.data.response.task

import com.themukha.taskforcebackend.model.Task
import java.time.LocalDateTime

data class TaskUpdateResponse(
    val id: Int,
    val title: String?,
    val description: String?,
    val updatedAt: LocalDateTime
)

fun Task.toUpdateResponse(): TaskUpdateResponse {
    return TaskUpdateResponse(
        id = id,
        title = title,
        description = description,
        updatedAt = updatedAt
    )
}