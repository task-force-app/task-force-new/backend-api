package com.themukha.taskforcebackend.data.request.workspace

data class WorkspaceCreateRequest(
    var name: String,
    var description: String? = null,
    var ownerId: Int,
)
