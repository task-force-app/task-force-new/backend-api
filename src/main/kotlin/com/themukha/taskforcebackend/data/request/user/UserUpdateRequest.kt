package com.themukha.taskforcebackend.data.request.user

data class UserUpdateRequest(
    var email: String? = null,
    var username: String? = null,
    var password: String? = null,
    var firstName: String? = null,
    var lastName: String? = null
)
