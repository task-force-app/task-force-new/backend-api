package com.themukha.taskforcebackend.data.request.task

data class TaskCreateRequest(
    val workspaceId: Int,
    val userId: Int,
    val title: String? = null,
    val description: String? = null
)
