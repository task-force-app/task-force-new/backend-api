package com.themukha.taskforcebackend.data.request.user

import java.time.LocalDateTime

data class UserCreateRequest(
    var email: String,
    var username: String,
    var password: String,
    var firstName: String? = null,
    var lastName: String? = null,
)
