package com.themukha.taskforcebackend.data.request.workspace

data class WorkspaceUpdateRequest(
    var name: String? = null,
    var description: String? = null
)
