package com.themukha.taskforcebackend.data.request.task

data class TaskUpdateRequest(
    val title: String? = null,
    val description: String? = null
)