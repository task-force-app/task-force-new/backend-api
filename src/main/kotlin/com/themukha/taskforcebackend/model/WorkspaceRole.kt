package com.themukha.taskforcebackend.model

data class WorkspaceRole(
    val workspaceId: Int,
    val roleId: Int,
    var isActive: Boolean = true
)