package com.themukha.taskforcebackend.model

import java.time.LocalDateTime

data class Workspace(
    val id: Int = 0,
    var name: String,
    var description: String? = null,
    var ownerId: Int,
    var isActive: Boolean = true,
    var createdAt: LocalDateTime = LocalDateTime.now(),
    var updatedAt: LocalDateTime = LocalDateTime.now()
)