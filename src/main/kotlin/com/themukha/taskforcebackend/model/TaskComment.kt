package com.themukha.taskforcebackend.model

import java.time.LocalDateTime

data class TaskComment(
    val id: Int = 0,
    var taskId: Int,
    var userId: Int,
    var text: String,
    var isEdited: Boolean = false,
    var createdAt: LocalDateTime = LocalDateTime.now(),
    var isActive: Boolean = true
)