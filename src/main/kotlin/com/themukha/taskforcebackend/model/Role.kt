package com.themukha.taskforcebackend.model

data class Role(
    val id: Int = 0,
    var name: String,
    var description: String? = null
)