package com.themukha.taskforcebackend.model

data class UserRole(
    val userId: Int,
    val roleId: Int,
    var isActive: Boolean = true
)