package com.themukha.taskforcebackend.model

import java.time.LocalDateTime

data class Task(
    val id: Int = 0,
    var workspaceId: Int,
    var userId: Int,
    var title: String? = null,
    var description: String? = null,
    var createdAt: LocalDateTime = LocalDateTime.now(),
    var updatedAt: LocalDateTime = LocalDateTime.now(),
    var isActive: Boolean = true
)