package com.themukha.taskforcebackend.model

data class UserWorkspaceRole(
    val userId: Int,
    val workspaceId: Int,
    val roleId: Int,
    var isActive: Boolean = true
)