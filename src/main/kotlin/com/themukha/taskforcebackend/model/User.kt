package com.themukha.taskforcebackend.model

import java.time.LocalDateTime

data class User(
    val id: Int = 0,
    var email: String,
    var username: String,
    var password: String,
    var firstName: String? = null,
    var lastName: String? = null,
    var isActive: Boolean = true,
    var createdAt: LocalDateTime = LocalDateTime.now(),
    var updatedAt: LocalDateTime = LocalDateTime.now()
)
