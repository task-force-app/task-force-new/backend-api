package com.themukha.taskforcebackend.model

import java.time.LocalDateTime

data class TaskHistory(
    val id: Int = 0,
    var taskId: Int,
    var title: String,
    var description: String? = null,
    var createdAt: LocalDateTime = LocalDateTime.now(),
    var updatedAt: LocalDateTime = LocalDateTime.now()
)