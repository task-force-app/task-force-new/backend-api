package com.themukha.taskforcebackend.util

import java.sql.ResultSet

fun ResultSet.getIntOrNull(columnLabel: String): Int? {
    val value = this.getInt(columnLabel)
    return if (this.wasNull()) null
    else value
}

fun ResultSet.getStringOrNull(columnLabel: String): String? {
    val value = this.getString(columnLabel)
    return if (this.wasNull()) null
    else value
}