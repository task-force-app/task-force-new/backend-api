package com.themukha.taskforcebackend.util

import org.slf4j.Logger
import org.slf4j.LoggerFactory

fun logger(javaClass: Any): Logger = LoggerFactory.getLogger(javaClass::class.java)