package com.themukha.taskforcebackend.dto

data class RoleDto(
    val id: Int? = null,
    var name: String? = null,
    var description: String? = null
)