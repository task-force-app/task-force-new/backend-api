package com.themukha.taskforcebackend.dto

import java.time.LocalDateTime

data class TaskHistoryDto(
    val id: Int? = null,
    var taskId: Int? = null,
    var title: String? = null,
    var description: String? = null,
    var createdAt: LocalDateTime = LocalDateTime.now(),
    var updatedAt: LocalDateTime = LocalDateTime.now()
)