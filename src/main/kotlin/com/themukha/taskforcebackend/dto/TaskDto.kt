package com.themukha.taskforcebackend.dto

import com.themukha.taskforcebackend.model.Task
import java.time.LocalDateTime

data class TaskDto(
    val id: Int? = null,
    var workspaceId: Int? = null,
    var userId: Int? = null,
    var title: String? = null,
    var description: String? = null,
    var createdAt: LocalDateTime = LocalDateTime.now(),
    var updatedAt: LocalDateTime = LocalDateTime.now(),
    var isActive: Boolean? = null
)

fun Task.toDto(): TaskDto {
    return TaskDto(
        id = id,
        workspaceId = workspaceId,
        userId = userId,
         title = title,
        description = description,
        createdAt = createdAt,
        updatedAt = updatedAt,
        isActive = isActive,
    )
}