package com.themukha.taskforcebackend.dto

import java.time.LocalDateTime

data class TaskCommentDto(
    val id: Int? = null,
    var taskId: Int? = null,
    var userId: Int? = null,
    var text: String? = null,
    var isEdited: Boolean? = null,
    var createdAt: LocalDateTime = LocalDateTime.now(),
    var isActive: Boolean? = null
)