package com.themukha.taskforcebackend.dto

data class UserRoleDto(
    val userId: Int? = null,
    val roleId: Int? = null,
    var isActive: Boolean = true
)