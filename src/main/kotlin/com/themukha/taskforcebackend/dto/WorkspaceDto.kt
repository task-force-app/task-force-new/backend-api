package com.themukha.taskforcebackend.dto

import com.themukha.taskforcebackend.model.Workspace
import java.time.LocalDateTime

data class WorkspaceDto(
    val id: Int? = null,
    var name: String? = null,
    var description: String? = null,
    var ownerId: Int? = null,
    var isActive: Boolean = true,
    var createdAt: LocalDateTime? = null,
    var updatedAt: LocalDateTime? = null
)

fun Workspace.toDto(): WorkspaceDto {
    return WorkspaceDto(
        id = id,
        name = name,
        description = description,
        ownerId = ownerId,
        isActive = isActive,
        createdAt = createdAt,
        updatedAt = updatedAt
    )
}