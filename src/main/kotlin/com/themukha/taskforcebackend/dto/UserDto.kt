package com.themukha.taskforcebackend.dto

import com.themukha.taskforcebackend.model.User
import java.time.LocalDateTime

data class UserDto (
    val id: Int = 0,
    var email: String? = null,
    var username: String? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var isActive: Boolean? = null,
    var createdAt: LocalDateTime = LocalDateTime.now(),
    var updatedAt: LocalDateTime = LocalDateTime.now()
)

fun User.toDto(): UserDto {
    return UserDto(
        id = id,
        email = email,
        username = username,
        firstName = firstName,
        lastName = lastName,
        isActive = isActive,
        createdAt = createdAt,
        updatedAt = updatedAt
    )
}