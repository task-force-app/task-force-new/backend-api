package com.themukha.taskforcebackend.dto

data class UserWorkspaceRoleDto(
    val userId: Int? = null,
    val workspaceId: Int? = null,
    val roleId: Int? = null,
    var isActive: Boolean? = null
)