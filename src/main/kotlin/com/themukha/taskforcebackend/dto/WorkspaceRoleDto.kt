package com.themukha.taskforcebackend.dto

data class WorkspaceRoleDto(
    val workspaceId: Int? = null,
    val roleId: Int? = null,
    var isActive: Boolean? = null
)