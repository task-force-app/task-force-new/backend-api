package com.themukha.taskforcebackend.service

import com.themukha.taskforcebackend.data.request.workspace.WorkspaceCreateRequest
import com.themukha.taskforcebackend.data.request.workspace.WorkspaceUpdateRequest
import com.themukha.taskforcebackend.data.response.workspace.WorkspaceCreateResponse
import com.themukha.taskforcebackend.data.response.workspace.WorkspaceUpdateResponse
import com.themukha.taskforcebackend.data.response.workspace.toCreateResponse
import com.themukha.taskforcebackend.data.response.workspace.toUpdateResponse
import com.themukha.taskforcebackend.dto.WorkspaceDto
import com.themukha.taskforcebackend.dto.toDto
import com.themukha.taskforcebackend.exception.WorkspaceCanNotBeCreatedException
import com.themukha.taskforcebackend.exception.WorkspaceCanNotBeDeletedException
import com.themukha.taskforcebackend.exception.WorkspaceCanNotBeUpdatedException
import com.themukha.taskforcebackend.exception.WorkspaceNotFoundException
import com.themukha.taskforcebackend.repository.WorkspaceRepository
import com.themukha.taskforcebackend.util.logger
import org.springframework.stereotype.Service

@Service
class WorkspaceServiceImpl(
    private val workspaceRepository: WorkspaceRepository,
) : WorkspaceService {
    override fun getAll(): List<WorkspaceDto> {
        val result = workspaceRepository.getAll()
        logger(this).info("Got all workspaces: `{}`", result)
        return result
            .map { it.toDto() }
    }

    override fun getById(id: Int): WorkspaceDto {
        val result = workspaceRepository.findById(id)
            ?: throw WorkspaceNotFoundException(id)
        logger(this).info("Got workspace by id: `{}`", result)
        return result.toDto()
    }

    override fun create(workspace: WorkspaceCreateRequest): WorkspaceCreateResponse {
        val result = try {
            workspaceRepository.create(workspace)
        } catch (e: Exception) {
            logger(this).error("Error creating workspace: {}", e.message)
            throw WorkspaceCanNotBeCreatedException(e.message ?: "Unknown error")
        }
        logger(this).info("Created workspace with id `{}`: `{}", result.id, result)
        return result.toCreateResponse()
    }

    override fun update(id: Int, workspace: WorkspaceUpdateRequest): WorkspaceUpdateResponse {
        val existingWorkspace = getById(id)  // Throws an exception if workspace doesn't exist
        val result = try {
            workspaceRepository.update(id, workspace)
        } catch (e: Exception) {
            logger(this).error("Error updating workspace with id {}: {}", id, e.message)
            throw WorkspaceCanNotBeUpdatedException(id, e.message ?: "Unknown error")
        }
        logger(this).info("Updated workspace by id `{}` from: `{}`, to: `{}`", id, existingWorkspace, result)
        return result.toUpdateResponse()
    }

    override fun deleteById(id: Int) {
        getById(id)
        try {
            workspaceRepository.deleteById(id)
            logger(this).info("Set workspace with id `{}` as `is_active = false`", id)
        } catch (e: Exception) {
            logger(this).error("Error deleting workspace with id {}: {}", id, e.message)
            throw WorkspaceCanNotBeDeletedException(id, e.message ?: "Unknown error")
        }
    }
}