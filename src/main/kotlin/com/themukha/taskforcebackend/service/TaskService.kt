package com.themukha.taskforcebackend.service

import com.themukha.taskforcebackend.data.request.task.TaskCreateRequest
import com.themukha.taskforcebackend.data.request.task.TaskUpdateRequest
import com.themukha.taskforcebackend.data.response.task.TaskCreateResponse
import com.themukha.taskforcebackend.data.response.task.TaskUpdateResponse
import com.themukha.taskforcebackend.dto.TaskDto

interface TaskService {
    fun getAll(): List<TaskDto>
    fun getById(id: Int): TaskDto
    fun create(task: TaskCreateRequest): TaskCreateResponse
    fun update(id: Int, updatedTask: TaskUpdateRequest): TaskUpdateResponse
    fun deleteById(id: Int)
}