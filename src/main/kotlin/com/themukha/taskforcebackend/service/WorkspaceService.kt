package com.themukha.taskforcebackend.service

import com.themukha.taskforcebackend.data.request.workspace.WorkspaceCreateRequest
import com.themukha.taskforcebackend.data.request.workspace.WorkspaceUpdateRequest
import com.themukha.taskforcebackend.data.response.workspace.WorkspaceCreateResponse
import com.themukha.taskforcebackend.data.response.workspace.WorkspaceUpdateResponse
import com.themukha.taskforcebackend.dto.WorkspaceDto

interface WorkspaceService {
    fun getAll(): List<WorkspaceDto>
    fun getById(id: Int): WorkspaceDto
    fun create(workspace: WorkspaceCreateRequest): WorkspaceCreateResponse
    fun update(id: Int, workspace: WorkspaceUpdateRequest): WorkspaceUpdateResponse
    fun deleteById(id: Int)
}