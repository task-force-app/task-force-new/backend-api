package com.themukha.taskforcebackend.service

import com.themukha.taskforcebackend.data.request.user.UserCreateRequest
import com.themukha.taskforcebackend.data.request.user.UserUpdateRequest
import com.themukha.taskforcebackend.data.response.user.UserCreateResponse
import com.themukha.taskforcebackend.data.response.user.UserUpdateResponse
import com.themukha.taskforcebackend.data.response.user.toUpdateResponse
import com.themukha.taskforcebackend.data.response.user.toCreateResponse
import com.themukha.taskforcebackend.dto.UserDto
import com.themukha.taskforcebackend.dto.toDto
import com.themukha.taskforcebackend.exception.UserNotFoundException
import com.themukha.taskforcebackend.repository.UserRepository
import com.themukha.taskforcebackend.util.logger
import org.springframework.stereotype.Service

@Service
class UserServiceImpl(
    private val userRepository: UserRepository,
) : UserService {
    override fun getAll(): List<UserDto> {
        val result = userRepository.getAll()
        logger(this).info("Got all users: `{}`", result)
        return result
            .map { it.toDto() }
    }

    override fun getById(id: Int): UserDto {
        val result = userRepository.findById(id)
            ?: throw UserNotFoundException(id)
        logger(this).info("Got user by id: `{}`", result)
        return result.toDto()
    }

    override fun create(user: UserCreateRequest): UserCreateResponse {
        val result = userRepository.create(user).toCreateResponse()
        logger(this).info("Created user by email `{}` with id: `{}`", user.email, result)
        return result
    }

    override fun update(id: Int, user: UserUpdateRequest): UserUpdateResponse {
        val existingUser = getById(id)
        val result = userRepository.update(id, user)
        logger(this).info("Updated user by id `{}` from: `{}`, to: `{}`", id, existingUser, result.toDto())
        return result.toUpdateResponse()
    }

    override fun deleteById(id: Int) {
        getById(id)
        userRepository.deleteById(id)
        logger(this).info("Set user with id {} as `is_active = false`", id)
    }
}