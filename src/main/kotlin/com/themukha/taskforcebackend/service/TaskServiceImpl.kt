package com.themukha.taskforcebackend.service

import com.themukha.taskforcebackend.data.request.task.TaskCreateRequest
import com.themukha.taskforcebackend.data.request.task.TaskUpdateRequest
import com.themukha.taskforcebackend.data.response.task.TaskCreateResponse
import com.themukha.taskforcebackend.data.response.task.TaskUpdateResponse
import com.themukha.taskforcebackend.data.response.task.toCreateResponse
import com.themukha.taskforcebackend.data.response.task.toUpdateResponse
import com.themukha.taskforcebackend.dto.TaskDto
import com.themukha.taskforcebackend.dto.toDto
import com.themukha.taskforcebackend.exception.TaskCanNotBeCreatedException
import com.themukha.taskforcebackend.exception.TaskNotFoundException
import com.themukha.taskforcebackend.repository.TaskRepository
import com.themukha.taskforcebackend.util.logger
import org.springframework.stereotype.Service

@Service
class TaskServiceImpl(
    private val taskRepository: TaskRepository
) : TaskService {
    override fun getAll(): List<TaskDto> {
        val result = taskRepository.findAll()
            .map { it.toDto() }
        logger(this).info("Got all tasks: `{}`", result)
        return result
    }

    override fun getById(id: Int): TaskDto {
        try {
            val result = taskRepository.findById(id)
                ?.toDto()
                ?: throw TaskNotFoundException(id)
            logger(this).info("Got task by id: `{}`", result)
            return result
        } catch (e: TaskNotFoundException) {
            logger(this).info("{}", e.message)
            throw e
        }
    }

    override fun create(task: TaskCreateRequest): TaskCreateResponse {
        try {
            val result = taskRepository.create(task)
            logger(this).info("Create task with id = `{}`: `{}`", result.id, result.toDto())
            return result.toCreateResponse()
        } catch (e: TaskCanNotBeCreatedException) {
            logger(this).error("{}", e.message)
            throw e
        }
    }

    override fun update(id: Int, updatedTask: TaskUpdateRequest): TaskUpdateResponse {
        val existingTask = getById(id)
        val result = taskRepository.update(id, updatedTask, existingTask)
        logger(this).info("Updated task by id `{}` from: `{}`, to: `{}`", id, existingTask, result.toDto())
        return result.toUpdateResponse()
    }

    override fun deleteById(id: Int) {
        taskRepository.deleteById(id)
        logger(this).info("Set task with id `{}` as `is_active = false`", id)
    }
}