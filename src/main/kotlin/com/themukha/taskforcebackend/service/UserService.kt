package com.themukha.taskforcebackend.service

import com.themukha.taskforcebackend.data.request.user.UserCreateRequest
import com.themukha.taskforcebackend.data.request.user.UserUpdateRequest
import com.themukha.taskforcebackend.data.response.user.UserCreateResponse
import com.themukha.taskforcebackend.data.response.user.UserUpdateResponse
import com.themukha.taskforcebackend.dto.UserDto

interface UserService {
    fun getAll(): List<UserDto>
    fun getById(id: Int): UserDto
    //    fun create(email: String, username: String, password: String, firstName: String, lastName: String): Int
    fun create(user: UserCreateRequest): UserCreateResponse
    fun update(id: Int, user: UserUpdateRequest): UserUpdateResponse
    fun deleteById(id: Int)
}