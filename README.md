### Error codes
| Error Code | Meaning                                           |
|------------|---------------------------------------------------|
| -1         | User not found while processing request           |
| -2         | User with specified email/username already exists |
| -3         | User cannot be updated                            |
| -4         | User cannot be deleted                            |
| -10        | Task with specified id not found                  |
| -11        | Task cannot be created in the specified workspace |
| -12        | Task cannot be updated                            |
| -13        | Task cannot be deleted                            |
| -20        | Workspace with specified id not found             |
| -21        | Workspace cannot be created                       |
| -22        | Workspace cannot be updated                       |
| -23        | Workspace cannot be deleted                       |
|            |                                                   |
|            |                                                   |
|            |                                                   |
|            |                                                   |
|            |                                                   |

## Developing new controller workflow

### Example for the `users` table

| Step          | Class                                                                                                  | Description                                                     |
|---------------|--------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------|
| 1. DTO        | [UserDto](./src/main/kotlin/com/themukha/taskforcebackend/dto/UserDto.kt)                              | Data Transfer Object for the new entity.                        |
| 2. Model      | [User](./src/main/kotlin/com/themukha/taskforcebackend/model/User.kt)                                  | Model class that represents the entity in the database.         |
| 3. Repository | [UserRepository](./src/main/kotlin/com/themukha/taskforcebackend/repository/UserRepository.kt)         | Interface that defines methods for accessing data.              |
|               | [UserRepositoryImpl](./src/main/kotlin/com/themukha/taskforcebackend/repository/UserRepositoryImpl.kt) | Implementation of the repository interface.                     |
| 4. Service    | [UserService](./src/main/kotlin/com/themukha/taskforcebackend/service/UserService.kt)                  | Interface that defines business logic.                          |
|               | [UserServiceImpl](./src/main/kotlin/com/themukha/taskforcebackend/service/UserServiceImpl.kt)          | Implementation of the service interface.                        |
| 5. Controller | [UserController](./src/main/kotlin/com/themukha/taskforcebackend/controller/UserController.kt)         | Controller that handles HTTP requests and uses the UserService. |




## After pulling the repository
> Add the environment variable into your system (Mac M1/M2):
> - `export TASKFORCE_POSTGRES_PASSWORD="{password}"`
> - `nano ~/.zshrc` - add to the last string `export TASKFORCE_POSTGRES_PASSWORD="{password}"`
> - `source ~/.zshrc `
>
> You can check if you completed the step:
> - `echo $TASKFORCE_POSTGRES_PASSWORD` - you will see the value

> Add the `.env` file to the root of project with variables:
>
> - `TASKFORCE_POSTGRES_PASSWORD={password}` - password of the Supabase database

## Where can I find env variables:
> # store env variables there:
# https://app.koyeb.com/secrets

## How to build the app:
> Example:
>
> `./gradlew clean build -Pversion={tag}`

## How to create the Docker image for the app:
> Example:
>
> `sudo docker image build --platform linux/amd64 -t georgymukha/task-force-backend:{tag} .`

## How to run the Docker container for the app locally:
> Example:
> `docker images` -> look at IMAGE ID`
> `docker run --env-file .env -p 8080:8080 -d {IMAGE ID}`


## FAQ
### ERROR [internal] load metadata for docker.io/library/openjdk:21-jdk
> - `docker pull openjdk:21`
> - `docker pull --platform linux/amd64 openjdk:21`

# [LICENSE](./license.md)