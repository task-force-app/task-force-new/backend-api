FROM openjdk:21
ARG JAR_FILE=build/libs/*.jar
WORKDIR /app
COPY ${JAR_FILE} task-force.jar
LABEL authors="georgymukha"
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "task-force.jar"]